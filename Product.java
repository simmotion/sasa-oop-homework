abstract public class Product {
    String name;
    Integer price;

    public Product() {}
    public Product(String name, Integer price) {
        this.name = name;
        this.price = price;
    }
}
