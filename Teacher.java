public class Teacher extends Person {
    public String school = "SASA";
    public String subject = "정보";

    public boolean isAdult() {
        return true;
    }
    public boolean checkOutsideOkay() {
        return true;
    }

    public Teacher() {
        super();
    }
    public Teacher(String name) {
        super(name);
    }
}
