public class SASAStudent extends Student {
    public boolean isAdult() {
        return false;
    }
    public boolean checkOutsideOkay() {
        return false;
    }

    public SASAStudent() { }
    public SASAStudent(String name) {
        super(name);
    }
}
