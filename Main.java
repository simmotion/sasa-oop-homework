import java.util.Scanner;

public class Main {
    public static void main(String[] args) throws Exception {
        Scanner sc = new Scanner(System.in);
        System.out.println("당신은 누구입니까?");
        System.out.println("1: 일반고 학생  2: SASA 학생  3: 선생님");
        int choose = sc.nextInt();

        if(choose<1 || choose>4) {
            System.out.println("잘못 골랐습니다.");
        }
        else {
            if(choose < 3) {
                Student me;
                System.out.print("이름을 알려주세요: ");
                String name = sc.next();
                if(choose == 1) me = new Student(name);
                else me = new SASAStudent(name);
                GSGoGo(me);
                cameBack(me);
            }
            else {
                Teacher me;
                System.out.print("성함을 알려주세요: ");
                String name = sc.next();
                me = new Teacher(name);
                GSGoGo(me);
                cameBack(me);
            }
        }
    }

    public static void GSGoGo(Student me) throws Exception {
        System.out.println(me.name + ": 난 편의점에 가고 있어~");
        goingToGS(me);
    }

    public static void GSGoGo(Teacher me) throws Exception {
        System.out.println(me.name + ": 나는 편의점에 가고 있다.");
        goingToGS(me);
    }

    public static void cameBack(Student me) {
        System.out.println(me.name + ": 편의점에 갔다 왔어!!");
    }
    public static void cameBack(Teacher me) {
        System.out.println(me.name + ": 편의점에 갔다 왔다.");
    }

    public static void goingToGS(Person me) throws Exception {
        Scanner sc = new Scanner(System.in);

        System.out.println("어디를 가시겠습니까?");
        System.out.println("1: GS25    2: 쿠팡");
        int choose = -1;
        while(choose<0 || choose>2) choose = sc.nextInt();

        if(choose == 1) {
            GSStore store = new GSStore();
            if(!store.personEnter(me)) return;
            store.buyWithPrompt();
        }
        else if(choose == 2) {
            CoupangStore store = new CoupangStore();
            if(!store.personEnter(me)) return;
            store.buyWithPrompt();
        }
    }
}
