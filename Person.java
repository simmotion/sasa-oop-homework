abstract public class Person {
    String name;
    public void sayHi() {
        if(this.isAdult()) System.out.println(String.format("나는 %s(이)다.", this.name));
        else System.out.println(String.format("안녕! 내 이름은 %s(이)야.", this.name));
    }

    public boolean isAdult() {
        return true;
    }
    public boolean checkOutsideOkay() {
        return true;
    }

    public Person() {}
    public Person(String name) {
        this.name = name;
        sayHi();
    }
}
