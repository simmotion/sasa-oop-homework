public class GSProduct extends Product {
    public boolean sale;

    public GSProduct(String name, Integer price, boolean sale) {
        this.name = name;
        this.price = price;
        this.sale = sale;
    }
}
