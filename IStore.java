public interface IStore {
    void buyItem(Integer index);
    boolean personEnter(Person person);
    void listProducts();
    void personLeave();
    void buyWithPrompt();
}
