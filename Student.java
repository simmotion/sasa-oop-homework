public class Student extends Person {
    public boolean isAdult() {
        return false;
    }
    public boolean checkOutsideOkay() {
        return true;
    }
    public Student() {
        super();
    }
    public Student(String name) {
        super(name);
    }
}
