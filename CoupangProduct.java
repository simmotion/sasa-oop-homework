public class CoupangProduct extends Product {
    public boolean rocketSend;

    public CoupangProduct(String name, Integer price, boolean rocketSend) {
        this.name = name;
        this.price = price;
        this.rocketSend = rocketSend;
    }
}
