import java.net.*;
import java.io.*;
import java.util.Scanner;
import java.util.regex.*;
import java.util.ArrayList;

public class CoupangStore implements IStore {
    Person customer;
    ArrayList<CoupangProduct> products;

    public CoupangStore() throws Exception {
        this.products = new ArrayList<CoupangProduct>();
        this.getProducts();
    }

    public boolean personEnter(Person person) {
        if (person.checkOutsideOkay() == false) {
            System.out.println("몰래 편의점에 왔다가 걸려서 벌점 5점을 받았다.");
            return false;
        }
        else {
            if(person.isAdult()) System.out.println("점원: 어서오세요! 쿠팡입니다.");
            else System.out.println("점원: ㅎㅇ");
        }
        this.customer = person;
        return true;
    }

    public void personLeave() {
        System.out.println("점원: 안녕히가세요~");
        return;
    }

    public void buyWithPrompt() {
        this.listProducts();
        Scanner sc = new Scanner(System.in);
        System.out.println("[ -1]: 나가기");
        while(true) {
            int sel = sc.nextInt();
            if(sel == -1) {
                personLeave();
                break;
            }
            else buyItem(sel-1);
        }
    }

    public void buyItem(Integer index) {
        if(index < 0 || index >= products.size() ) {
            System.out.println("유효하지 않은 상품 번호입니다.");
            return;
        }
        CoupangProduct product = products.get(index);
        System.out.println(
                String.format("%s(는)은 %s를 %d원에 샀다!",customer.name,product.name,product.price)
        );
    }

    public void listProducts() {
        System.out.println(String.format("편의점에 %d개의 상품이 있습니다.\n============================", products.size()));
        for(int i=0;i<products.size();i++) {
            System.out.print(String.format("[%3d]: %s: %d원", i+1, products.get(i).name, products.get(i).price));
            if(products.get(i).rocketSend) System.out.println("    <로켓배송!>");
            else System.out.println("");
        }
    }

    private void getProducts() throws Exception {
        System.out.println("상품 정보를 가져오는중...");
        for(int i=1;i<=2;i++) {
            String url = String.format("https://m.coupang.com/nm/plp/227?page=%d",i);
            URL page = new URL(url);

            URLConnection conn = page.openConnection();
            BufferedReader br = new BufferedReader(new InputStreamReader(conn.getInputStream()));

            String htmlFile="", inputLine;
            while ((inputLine = br.readLine()) != null) {
                htmlFile += inputLine;
            }
            br.close();

            Pattern p = Pattern.compile("<div class=\"subscription-info\">(.*?)<span class=\"promise-delivery-date\">");
            Matcher m = p.matcher(htmlFile);

            while(m.find()) {
                String productName, productPrice;
                Boolean canRocket;
                String productText = m.group(0);

                Pattern namePattern = Pattern.compile("<strong class=\"title\">(.*?)</strong>");
                Matcher nameMatcher = namePattern.matcher(productText);
                nameMatcher.find();
                productName = nameMatcher.group(1);

                Pattern pricePattern = Pattern.compile("<strong>(.*?)</strong>");
                Matcher priceMatcher = pricePattern.matcher(productText);
                priceMatcher.find();
                productPrice = priceMatcher.group(1).replace(",","");

                canRocket = productText.contains("rocket/rocket_logo.png");

                products.add(new CoupangProduct(productName, Integer.parseInt(productPrice), canRocket));
            }
        }
        products.sort((o1, o2) -> o1.price - o2.price);
        System.out.println("상품을 모두 가져왔습니다.");
        /*
        for(int i=0;i<products.size();i++) {
            System.out.println(products.get(i).name);
            System.out.println(products.get(i).price);
        }
        */
    }
}
