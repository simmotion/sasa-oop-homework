import java.util.Scanner;
import java.util.ArrayList;

public class GSStore implements IStore {
    Person customer;
    ArrayList<GSProduct> products;

    public GSStore() throws Exception {
        this.products = new ArrayList<GSProduct>();
        this.getProducts();
    }

    public boolean personEnter(Person person) {
        if (person.checkOutsideOkay() == false) {
            System.out.println("몰래 편의점에 왔다가 걸려서 벌점 5점을 받았다.");
            return false;
        } else System.out.println("점원: 어서오세요! GS25입니다.");
        this.customer = person;
        return true;
    }

    public void personLeave() {
        System.out.println("점원: 안녕히가세요~");
        return;
    }

    public void buyWithPrompt() {
        this.listProducts();
        Scanner sc = new Scanner(System.in);
        System.out.println("[ -1]: 나가기");
        while(true) {
            int sel = sc.nextInt();
            if(sel == -1) {
                personLeave();
                break;
            }
            else buyItem(sel-1);
        }
    }

    public void buyItem(Integer index) {
        if(index < 0 || index >= products.size() ) {
            System.out.println("유효하지 않은 상품 번호입니다.");
            return;
        }
        GSProduct product = products.get(index);
        System.out.println(
                String.format("%s(는)은 %s를 %d원에 샀다!",customer.name,product.name,product.price)
        );
    }

    public void listProducts() {
        System.out.println(String.format("편의점에 %d개의 상품이 있습니다.\n============================", products.size()));
        for(int i=0;i<products.size();i++) {
            System.out.print(String.format("[%3d]: %s: %d원", i+1, products.get(i).name, products.get(i).price));
            if(products.get(i).sale) System.out.println("    <세일중>");
            else System.out.println("");
        }
    }

    private void getProducts() throws Exception {
        System.out.println("상품 정보를 가져오는중...");

        products.add(new GSProduct("삼다수", 500, false));
        products.add(new GSProduct("라면", 1000, false));
        products.add(new GSProduct("양말", 1500, false));
        products.add(new GSProduct("민트초코우유", 1200, true));
        products.add(new GSProduct("삼각김밥", 750, false));
        products.add(new GSProduct("아이폰", 75000, true));
        products.add(new GSProduct("히오스 이용권", 1, false));

        System.out.println("상품을 모두 가져왔습니다.");
    }
}
